# Az alkalmazásról
A PiedWallet egy könyvelő alkalmazás, amely Android operációs rendszeren való használatra lett kifejlesztve.
A program segítségével egyszerűen vehet fel pénzügyi tranzakciókat, kiadásokat és bevételeket egyaránt, amelyeket különböző kategóriákba lehet sorolni, majd azok szerint áttekinthetők a betáplált adatok.

## Continuous Integration
A CI megvalósításához a Bitbucket Pipelines technológiáját használtuk. A bitbucket oldalon a baloldali menüsorban a Pipelines menüpont alatt tekinethetőek meg a rendszer által rögzített build és teszt előzmények.
## Javadoc
A klónozott mappa doc/index.html megnyitásával tekinthető meg a generált kóddokumentáció.
## Dokumentáció
Plusz segítséget nyújt a projekt beüzemelésével kapcsolatban a Dokumentáció.pdf fájl.

## Tesztelés
### Linux 
./gradlew test
### Windows (cmd)
.\gradlew test
### Android Studio
jobb click a project explorerben (bal oldalt az IDE-ben) az app/java/com.piedpiper_elte.piedwallet (test) mappára és run 'Tests in 'piedwallet''