package com.piedpiper_elte.piedwallet;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class TransactionCategorySumTest {

    private TransactionCategorySum getInstance() {
        List<TransactionEntity> list = new ArrayList<>();
        TransactionEntity entity1 = new TransactionEntity();
        entity1.setBalanceChange(-500);
        entity1.setCategoryStringId(Category.FOOD_DRINKS.getStringId());
        entity1.setId(0);
        entity1.setTitle("Teszt1");
        list.add(entity1);

        TransactionEntity entity2 = new TransactionEntity();
        entity2.setBalanceChange(-1500);
        entity2.setCategoryStringId(Category.FOOD_DRINKS.getStringId());
        entity2.setId(1);
        entity2.setTitle("Teszt2");
        list.add(entity2);

        return new TransactionCategorySum(list);
    }

    @Test
    public void getCategoryStringId() {
        assertEquals(Category.FOOD_DRINKS.getStringId(), getInstance().getCategoryStringId());
    }

    @Test
    public void getCategory() {
        assertEquals(Category.FOOD_DRINKS, getInstance().getCategory());
    }

    @Test
    public void getSum() {
        assertEquals(-2000, getInstance().getSum());
    }

    @Test
    public void getIncome() {
        assertEquals(0, getInstance().getIncome());
    }

    @Test
    public void getOutlay() {
        assertEquals(-2000, getInstance().getOutlay());
    }
}