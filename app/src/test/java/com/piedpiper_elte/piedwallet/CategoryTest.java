package com.piedpiper_elte.piedwallet;

import org.junit.Test;

import static org.junit.Assert.*;

public class CategoryTest {

    @Test
    public void getByStringId() {
        assertEquals(Category.FOOD_DRINKS, Category.getByStringId(R.string.food_drinks));
        assertEquals(Category.SHOPPING, Category.getByStringId(R.string.shopping));
        assertEquals(Category.HOUSING, Category.getByStringId(R.string.housing));
        assertEquals(Category.TRANSPORTATION, Category.getByStringId(R.string.transportation));
        assertEquals(Category.VEHICLE, Category.getByStringId(R.string.vehicle));
        assertEquals(Category.LIFE_ENTERTAINMENT, Category.getByStringId(R.string.life_entertainment));
        assertEquals(Category.COMMUNICATION, Category.getByStringId(R.string.communication));
        assertEquals(Category.FINANCIAL_EXPENSES, Category.getByStringId(R.string.financial_expenses));
        assertEquals(Category.INVESTMENTS, Category.getByStringId(R.string.investments));
        assertEquals(Category.INCOME, Category.getByStringId(R.string.income));
        assertEquals(Category.OTHERS, Category.getByStringId(R.string.others));
    }
}