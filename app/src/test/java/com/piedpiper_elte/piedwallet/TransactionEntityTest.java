package com.piedpiper_elte.piedwallet;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class TransactionEntityTest {
    private TransactionEntity te;
    TransactionEntity te_notInitialized;

    @Before
    public void initTest() {
        this.te = new TransactionEntity();
    }

    @Test
    public void test_ID() {
        te.setId(0);
        assertEquals(te.getId(), 0);

        te.setId(1);
        assertEquals(te.getId(), 1);
    }

    @Test
    public void test_BalanceChange() {
        te.setBalanceChange(0);
        assertEquals(te.getBalanceChange(), 0);

        te.setBalanceChange(1);
        assertEquals(te.getBalanceChange(), 1);
    }

    @Test
    public void test_CategoryStringId() {
        te.setCategoryStringId(0);
        assertEquals(te.getCategoryStringId(), 0);

        te.setCategoryStringId(1);
        assertEquals(te.getCategoryStringId(), 1);
    }

    @Test
    public void test_Title() {
        assertEquals(te.getTitle(), null);

        te.setTitle("Teszt1");
        assertEquals(te.getTitle(), "Teszt1");

        te.setTitle("Teszt2");
        assertEquals(te.getTitle(), "Teszt2");
    }

    @Test
    public void test_Select() {
        assertEquals(te.isSelected(), false);

        te.setSelected(true);
        assertEquals(te.isSelected(), true);

        te.setSelected(false);
        assertEquals(te.isSelected(), false);
    }

    @Test(expected = NullPointerException.class)
    public void test_missingTitle() {
        TransactionEntity te_local = new TransactionEntity();
        String fail = te_local.getTitle();
        fail.length();
    }

    @Test(expected = NullPointerException.class)
    public void test_notInitialized() {
        te_notInitialized.getTitle();
    }
}