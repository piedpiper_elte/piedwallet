package com.piedpiper_elte.piedwallet;

import org.junit.Before;
import org.junit.Test;

import static com.piedpiper_elte.piedwallet.Utils.*;
import static org.junit.Assert.assertEquals;

public class UtilsTest {

    // Meg kell hívni az Utils konstruktorát, hogy elinduljanak a szükséges dolgok.
    @Before
    public void initUnitTest() {
        new Utils();
    }

    @Test
    public void test_generateHtmlTemplate() {
        assertEquals("<span style='color: #006400;'>0</span>", generateHtmlTemplate(0, false));
        assertEquals("<span style='color: #006400;'>0</span> Ft", generateHtmlTemplate(0, true));

        assertEquals("<span style='color: #8B0000;'>-1</span>", generateHtmlTemplate(-1, false));
        assertEquals("<span style='color: #8B0000;'>-1</span> Ft", generateHtmlTemplate(-1, true));

        assertEquals("<span style='color: #006400;'>+1</span>", generateHtmlTemplate(1, false));
        assertEquals("<span style='color: #006400;'>+1</span> Ft", generateHtmlTemplate(1, true));

        assertEquals("<span style='color: #006400;'>+2 147 483 647</span>", generateHtmlTemplate(Integer.MAX_VALUE, false));
        assertEquals("<span style='color: #006400;'>+2 147 483 647</span> Ft", generateHtmlTemplate(Integer.MAX_VALUE, true));

        assertEquals("<span style='color: #8B0000;'>-2 147 483 648</span>", generateHtmlTemplate(Integer.MIN_VALUE, false));
        assertEquals("<span style='color: #8B0000;'>-2 147 483 648</span> Ft", generateHtmlTemplate(Integer.MIN_VALUE, true));
    }

    @Test
    public void test_dfFormat() {
        assertEquals("0", dfFormat(0));
        assertEquals("-1", dfFormat(-1));

        assertEquals("543 253 243", dfFormat(543253243));
        assertEquals("-543 253 243", dfFormat(-543253243));

        assertEquals("2 147 483 647", dfFormat(Integer.MAX_VALUE));
        assertEquals("-2 147 483 648", dfFormat(Integer.MIN_VALUE));
    }

    /*
    @Test(expected = NumberFormatException.class)
    public void numberFormatException() {

    }*/
}
