package com.piedpiper_elte.piedwallet;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;

public class Utils {
    private static DecimalFormat df;
    private static DecimalFormatSymbols symbols;

    public Utils() {
        initializeFormatter();
    }

    /**
     * HTML template létrehozása
     * @param number szám
     * @param addCurrency ha true, megjelenik, hogy "Ft"
     * @return
     */
    public static String generateHtmlTemplate(int number, boolean addCurrency) {
        StringBuilder sb = new StringBuilder();
        if (number >= 0) {
            sb.append("<span style='color: #006400;'>");
            if (number > 0) sb.append("+");
        }
        else {
            sb.append("<span style='color: #8B0000;'>");
        }
        sb.append(dfFormat(number));
        sb.append("</span>");
        if (addCurrency) sb.append(" Ft");

        return sb.toString();
    }

    /**
     * Számformatter inicializálása
     */
    public static void initializeFormatter() {
        symbols = new DecimalFormatSymbols();
        symbols.setGroupingSeparator(' ');

        df = new DecimalFormat();
        df.setDecimalFormatSymbols(symbols);
        df.setGroupingSize(3);
        df.setMaximumFractionDigits(0);
    }

    /**
     * Decimális formázás
     * @param number szám
     * @return formázott szám String
     */
    public static String dfFormat(int number) {
        return df.format(number);
    }
}
