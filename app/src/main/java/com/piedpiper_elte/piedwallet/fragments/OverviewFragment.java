package com.piedpiper_elte.piedwallet.fragments;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.piedpiper_elte.piedwallet.R;
import com.piedpiper_elte.piedwallet.TransactionCategorySum;
import com.piedpiper_elte.piedwallet.TransactionCategorySumAdapter;

import java.util.ArrayList;

public class OverviewFragment extends Fragment {

    private Activity activity;
    private ArrayList<TransactionCategorySum> transactionCategorySumData = new ArrayList<>();
    private ArrayList<TransactionCategorySum> sortedTransactionCategories = new ArrayList<>();

    /**
     * Fragment UI elemeinek létrehozása
     * @param inflater Android által kezelt paraméter
     * @param container Android által kezelt paraméter
     * @param savedInstanceState Android által kezelt paraméter
     * @return létrehozott View
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_overview, container, false);
    }

    /**
     * Fragment UI elemeinek létrejötte után kerül meghívásra
     * @param view Android által kezelt paraméter
     * @param savedInstanceState Android által kezelt paraméter
     */
    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init();
    }

    /**
     * Betölti az adatokat, meghívja az adatok feldolgozását
     */
    public void init() {
        ArrayList<String> transactionStrings = null;
        Bundle bundle = getArguments();
        if (bundle != null) {
            transactionStrings = bundle.getStringArrayList("transactionCategorySums");
            for (String act : transactionStrings) {
                transactionCategorySumData.add(new TransactionCategorySum(act));
            }
        }

        noTransactionsMessage();

        if(!transactionCategorySumData.isEmpty()) {
            sortedTransactionCategories = sortTransactionCategorySumsByOutlayable();
            calculateData();
        }
    }

    /**
     * Kiválogatja az adatokból azokat, melyeket érdemes megjeleníteni, Outlayable alapján
     * @return válogatott adatok
     */
    private ArrayList<TransactionCategorySum> sortTransactionCategorySumsByOutlayable() {
        ArrayList<TransactionCategorySum> objects = new ArrayList<TransactionCategorySum>();
        for (TransactionCategorySum act : transactionCategorySumData) {
            if (act.isOutlayable() && !(act.getOutlay() == 0 && act.getIncome() == 0)) {
                objects.add(act);
            }
        }
        for (TransactionCategorySum act : transactionCategorySumData) {
            if (!act.isOutlayable() && !(act.getOutlay() == 0 && act.getIncome() == 0)) {
                objects.add(act);
            }
        }
        return objects;
    }

    /**
     * Statisztikát gyárt a tranzakcióadatokból
     */
    private void calculateData() {
        TransactionCategorySum mostSpend = transactionCategorySumData.get(0);
        TransactionCategorySum leastSpend = transactionCategorySumData.get(0);
        TransactionCategorySum mostIncome = transactionCategorySumData.get(0);
        TransactionCategorySum mostTransactions = transactionCategorySumData.get(0);
        for(TransactionCategorySum categorySum : transactionCategorySumData) {
            if(categorySum.getOutlay() < mostSpend.getOutlay()) mostSpend = categorySum;
            if(categorySum.getOutlay() != 0 && categorySum.getOutlay() > leastSpend.getOutlay()) leastSpend = categorySum;
            if(categorySum.getIncome() > mostIncome.getIncome()) mostIncome = categorySum;
            if(categorySum.getTransactionCount() > mostTransactions.getTransactionCount()) mostTransactions = categorySum;
        }
        ((TextView) getView().findViewById(R.id.category_mostspend)).setText(activity.getResources().getString(mostSpend.getCategoryStringId()));
        ((ImageView) getView().findViewById(R.id.imageView_mostspend)).setImageResource(mostSpend.getCategory().getDrawableId());
        ((TextView) getView().findViewById(R.id.sum_mostspend)).setText(Integer.toString(mostSpend.getOutlay()));
        ((TextView) getView().findViewById(R.id.transcount_mostspend)).setText(Integer.toString(mostSpend.getTransactionCount()));

        ((TextView) getView().findViewById(R.id.category_leastspend)).setText(activity.getResources().getString(leastSpend.getCategory().getStringId()));
        ((ImageView) getView().findViewById(R.id.imageView_leastspend)).setImageResource(leastSpend.getCategory().getDrawableId());
        ((TextView) getView().findViewById(R.id.sum_leastspend)).setText(Integer.toString(leastSpend.getOutlay()));
        ((TextView) getView().findViewById(R.id.transcount_leastspend)).setText(Integer.toString(leastSpend.getTransactionCount()));

        ((TextView) getView().findViewById(R.id.category_mostincome)).setText(activity.getResources().getString(mostIncome.getCategory().getStringId()));
        ((ImageView) getView().findViewById(R.id.imageView_mostincome)).setImageResource(mostIncome.getCategory().getDrawableId());
        ((TextView) getView().findViewById(R.id.sum_mostincome)).setText(Integer.toString(mostIncome.getIncome()));
        ((TextView) getView().findViewById(R.id.transcount_mostincome)).setText(Integer.toString(mostIncome.getTransactionCount()));

        ((TextView) getView().findViewById(R.id.category_mosttrans)).setText(activity.getResources().getString(mostTransactions.getCategory().getStringId()));
        ((ImageView) getView().findViewById(R.id.imageView_mosttrans)).setImageResource(mostTransactions.getCategory().getDrawableId());
        ((TextView) getView().findViewById(R.id.sum_mosttrans)).setText(Integer.toString(mostTransactions.getIncome() + mostTransactions.getOutlay()));
        ((TextView) getView().findViewById(R.id.transcount_mosttrans)).setText(Integer.toString(mostTransactions.getTransactionCount()));
    }

    /**
     * Ha nincs az adatbázisban tranzakció, megjelenít egy erre vonatkozó üzenetet
     */
    private void noTransactionsMessage() {
        TextView noTransactions = getView().findViewById(R.id.noTransactionsTextView);
        noTransactions.setVisibility(View.INVISIBLE);
        getView().findViewById(R.id.cardView_mostspend).setVisibility(View.VISIBLE);
        getView().findViewById(R.id.cardView_leastspend).setVisibility(View.VISIBLE);
        getView().findViewById(R.id.cardView_mostincome).setVisibility(View.VISIBLE);
        getView().findViewById(R.id.cardView_mosttrans).setVisibility(View.VISIBLE);
        if (transactionCategorySumData.isEmpty()) {
            noTransactions.setVisibility(View.VISIBLE);
            getView().findViewById(R.id.cardView_mostspend).setVisibility(View.INVISIBLE);
            getView().findViewById(R.id.cardView_leastspend).setVisibility(View.INVISIBLE);
            getView().findViewById(R.id.cardView_mostincome).setVisibility(View.INVISIBLE);
            getView().findViewById(R.id.cardView_mosttrans).setVisibility(View.INVISIBLE);
        }
    }

    /**
     * Akkor hívódik meg, ha csatlakozott a szülő Activityhez
     * @param context szülő Context
     */
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof Activity) {
            this.activity = (Activity) context;
        }
    }

    /**
     * Akkor hívódik meg, ha lecsatlakozott a szülő Activityről
     */
    @Override
    public void onDetach() {
        super.onDetach();
        activity = null;
    }
}