package com.piedpiper_elte.piedwallet.fragments;

import android.app.Activity;
import android.arch.persistence.room.Room;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.piedpiper_elte.piedwallet.AppDatabase;
import com.piedpiper_elte.piedwallet.R;
import com.piedpiper_elte.piedwallet.TransactionAdapter;
import com.piedpiper_elte.piedwallet.TransactionEntity;

import java.util.ArrayList;
import java.util.List;

public class TransactionsFragment extends Fragment {

    private Activity activity;
    private AppDatabase db;
    private List<TransactionEntity> transactionData = new ArrayList<>();
    private List<TransactionEntity> selectedTransactions = new ArrayList<>();
    private TransactionAdapter adapter;
    private Toast deleteErrorToast;
    private TextView noTransactions;

    /**
     * Létrehozáskor hívódik meg, jelzi, hogy használja az Option Menu-t a Fragment
     * @param savedInstanceState
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true); // To access the delete button
    }

    /**
     * Fragment UI elemeinek létrehozása
     * @param inflater Android által kezelt paraméter
     * @param container Android által kezelt paraméter
     * @param savedInstanceState Android által kezelt paraméter
     * @return létrehozott View
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_transactions, container, false);
    }

    /**
     * Fragment UI elemeinek létrejötte után kerül meghívásra
     * @param view Android által kezelt paraméter
     * @param savedInstanceState Android által kezelt paraméter
     */
    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init();
    }

    /**
     * Betölti az adatokat, inicializálja néhány UI elemet
     */
    public void init() {
        db = Room.databaseBuilder(activity, AppDatabase.class, "database").allowMainThreadQueries().build();

        Bundle bundle = getArguments();
        if (bundle != null) {
            transactionData = (List<TransactionEntity>) bundle.getSerializable("transactionData");
        }

        // Ha nincsenek tranzakciók, akkor jelenítsünk meg róla egy tájékoztatást.
        noTransactions = getView().findViewById(R.id.noTransactionsTextView);
        noTransactionsMessage();

        deleteErrorToast = Toast.makeText(activity, R.string.delete_error, Toast.LENGTH_SHORT);

        adapter = new TransactionAdapter(activity, R.layout.row_item, transactionData);

        ListView listView = getView().findViewById(R.id.transactionsListView);
        listView.setAdapter(adapter);
        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {

            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long arg3) {
                TransactionEntity transactionEntity = transactionData.get(position);
                if (!transactionEntity.isSelected()) {
                    transactionEntity.setSelected(true);
                    selectedTransactions.add(transactionEntity);
                } else {
                    transactionEntity.setSelected(false);
                    selectedTransactions.remove(transactionEntity);
                }
                adapter.notifyDataSetChanged();
                return true;
            }
        });
    }

    /**
     * Törli a kiválasztott tranzakciókat
     */
    private void deleteSelectedTransactions() {
        for (TransactionEntity transactionEntity : selectedTransactions) {
            db.transactionDao().delete(transactionEntity);
            transactionData.remove(transactionEntity);
        }
        selectedTransactions.clear();
        adapter.notifyDataSetChanged();

        noTransactionsMessage();
    }

    /**
     * Törlés gomb megnyomásakor hívódik meg, létrahozza a törlés dialógust
     * @param item
     * @return
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_delete) {
            if (selectedTransactions.isEmpty()) {
                deleteErrorToast.show();
            } else {
                // Kérdezzük meg, hogy biztos ki akarja-e törölni a tranzakciókat
                createDeleteConfirmationDialog();
                //deleteSelectedTransactions();
            }
            return true;
        } else if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Megjelenít egy dialógust, ahol a felhasználónak meg kell erősítenie a törlési szándékát.
     */
    private void createDeleteConfirmationDialog() {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        builder.setTitle(R.string.confirm);

        // Attól függően jelenítjük meg az üzenetet, hogy csak 1 vagy több elemet jelölt ki.
        builder.setMessage(selectedTransactions.size() > 1 ? R.string.delete_confirm_multiple_msg : R.string.delete_confirm_msg);

        builder.setNegativeButton(
                R.string.no,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                }
        );

        builder.setPositiveButton(
                R.string.yes,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        deleteSelectedTransactions();
                    }
                }
        );

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    /**
     * Ha nincs az adatbázisban tranzakció, megjelenít egy erre vonatkozó üzenetet
     */
    private void noTransactionsMessage() {
        noTransactions.setVisibility(View.INVISIBLE);
        if (transactionData.isEmpty()) {
            noTransactions.setVisibility(View.VISIBLE);
        }
    }

    /**
     * Akkor hívódik meg, ha csatlakozott a szülő Activityhez
     * @param context szülő Context
     */
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof Activity) {
            this.activity = (Activity) context;
        }
    }

    /**
     * Akkor hívódik meg, ha lecsatlakozott a szülő Activityről
     */
    @Override
    public void onDetach() {
        super.onDetach();
        activity = null;
    }
}