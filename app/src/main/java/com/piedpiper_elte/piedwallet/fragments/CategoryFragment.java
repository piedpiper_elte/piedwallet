package com.piedpiper_elte.piedwallet.fragments;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.piedpiper_elte.piedwallet.R;
import com.piedpiper_elte.piedwallet.TransactionCategorySum;
import com.piedpiper_elte.piedwallet.TransactionCategorySumAdapter;

import java.util.ArrayList;

public class CategoryFragment extends Fragment {

    private Activity activity;
    private ArrayList<TransactionCategorySum> transactionCategorySumData = new ArrayList<>();
    private TransactionCategorySumAdapter adapter;

    /**
     * Fragment UI elemeinek létrehozása
     * @param inflater Android által kezelt paraméter
     * @param container Android által kezelt paraméter
     * @param savedInstanceState Android által kezelt paraméter
     * @return létrehozott View
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_category, container, false);
    }

    /**
     * Fragment UI elemeinek létrejötte után kerül meghívásra
     * @param view Android által kezelt paraméter
     * @param savedInstanceState Android által kezelt paraméter
     */
    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init();
    }

    /**
     * Adatok betöltése és feldolgozása megjelenítés előtt
     */
    public void init() {
        transactionCategorySumData();
        noTransactionsMessage();
        initTransactionCategoryListView();
    }

    /**
     * Adatok betöltése Bundle-ből
     */
    private void transactionCategorySumData() {
        ArrayList<String> extra = null;
        Bundle bundle = getArguments();
        if (bundle != null) {
            extra = bundle.getStringArrayList("transactionCategorySums");
            for (String act : extra) {
                transactionCategorySumData.add(new TransactionCategorySum(act));
            }
        }
    }

    /**
     * Kiválogatja az adatokból azokat, melyeket érdemes megjeleníteni, Outlayable alapján
     * @return válogatott adatok
     */
    private ArrayList<TransactionCategorySum> sortTransactionCategorySumsByOutlayable() {
        ArrayList<TransactionCategorySum> objects = new ArrayList<TransactionCategorySum>();
        for (TransactionCategorySum act : transactionCategorySumData) {
            if (act.isOutlayable() && !(act.getOutlay() == 0 && act.getIncome() == 0)) {
                objects.add(act);
            }
        }
        for (TransactionCategorySum act : transactionCategorySumData) {
            if (!act.isOutlayable() && !(act.getOutlay() == 0 && act.getIncome() == 0)) {
                objects.add(act);
            }
        }
        return objects;
    }

    /**
     * ListView feltöltése adattal
     */
    private void initTransactionCategoryListView() {
        adapter = new TransactionCategorySumAdapter(activity, R.layout.category_row_item, sortTransactionCategorySumsByOutlayable());

        ListView listView = getView().findViewById(R.id.catlistview); // only works after calling onCreateView()
        listView.setAdapter(adapter);
    }

    /**
     * Ha nincs az adatbázisban tranzakció, megjelenít egy erre vonatkozó üzenetet
     */
    private void noTransactionsMessage() {
        TextView noTransactions = getView().findViewById(R.id.noTransactionsTextView);
        noTransactions.setVisibility(View.INVISIBLE);
        if (transactionCategorySumData.isEmpty()) {
            noTransactions.setVisibility(View.VISIBLE);
        }
    }

    /**
     * Akkor hívódik meg, ha csatlakozott a szülő Activityhez
     * @param context szülő Context
     */
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof Activity){
            this.activity = (Activity) context;
        }
    }

    /**
     * Akkor hívódik meg, ha lecsatlakozott a szülő Activityről
     */
    @Override
    public void onDetach() {
        super.onDetach();
        activity = null;
    }
}
