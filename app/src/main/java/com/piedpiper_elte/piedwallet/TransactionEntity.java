package com.piedpiper_elte.piedwallet;

import android.arch.persistence.room.*;

import java.io.Serializable;

@Entity
public class TransactionEntity implements Serializable {

    @PrimaryKey(autoGenerate = true)
    private int id;

    @ColumnInfo(name = "balanceChange")
    private int balanceChange;

    @ColumnInfo(name = "title")
    private String title;

    @ColumnInfo(name = "category")
    private int categoryStringId;

    @Ignore
    private boolean selected = false;

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setBalanceChange(int balanceChange) {
        this.balanceChange = balanceChange;
    }

    public int getBalanceChange() {
        return balanceChange;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public void setCategoryStringId(int categoryStringId) {
        this.categoryStringId = categoryStringId;
    }

    public int getCategoryStringId() {
        return categoryStringId;
    }

    public Category getCategory() {
        return Category.getByStringId(categoryStringId);
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }
}
