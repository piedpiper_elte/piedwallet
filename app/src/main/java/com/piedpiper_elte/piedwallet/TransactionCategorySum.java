package com.piedpiper_elte.piedwallet;


import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

public class TransactionCategorySum {
    private int income;
    private int outlay;
    private int categoryStringId;
    private Category category;
    private boolean outlayable = true;
    private int transactionCount;

    /**
     * Több tranzakciót összesít, összeadja, mennyi kiadás és bevétel jött belőle összesen, hány tranzakció volt
     * @param transactions
     */
    public TransactionCategorySum(List<TransactionEntity> transactions)
    {
        income = 0;
        outlay = 0;
        categoryStringId = transactions.get(0).getCategoryStringId();
        for(TransactionEntity act : transactions) {
            if(act.getBalanceChange() < 0) {
                outlay += act.getBalanceChange();
            }
            else {
                income += act.getBalanceChange();
            }
        }
        category = Category.getByStringId(categoryStringId);
        setOutlayable();
        transactionCount = transactions.size();
        //outlay *= -1;
    }

    private void setOutlayable()
    {
        if(category.equals(Category.INCOME))
        {
            outlayable = false;
        }
    }

    /**
     * Serialized Stringből hoz létre objektumot
     * @param inp
     */
    public TransactionCategorySum(String inp)
    {
        String[] splitted = inp.split(",");
        income = Integer.parseInt(splitted[0]);
        outlay = Integer.parseInt(splitted[1]);
        categoryStringId = Integer.parseInt(splitted[2]);
        transactionCount = Integer.parseInt(splitted[3]);
        category = Category.getByStringId(categoryStringId);
        setOutlayable();
    }


    public void setTitle(String title) {
        //this.title = title;
    }

    /**
     * Az adott kategória tartalmazhat-e kiadást
     * @return
     */
    public boolean isOutlayable() {
        return outlayable;
    }

    public void setCategoryStringId(int categoryStringId) {
        this.categoryStringId = categoryStringId;
    }

    public int getCategoryStringId() {
        return categoryStringId;
    }

    public Category getCategory() {
        return Category.getByStringId(categoryStringId);
    }

    public int getSum() {
        return income + outlay;
    }

    public int getIncome() {
        return income;
    }

    public void setIncome(int income) {
        this.income = income;
    }

    public int getOutlay() {
        return outlay;
    }

    public void setOutlay(int income) {
        this.outlay = income;
    }

    public int getTransactionCount() {
        return transactionCount;
    }

    public void setTransactionCount(int transactionCount) {
        this.transactionCount = transactionCount;
    }

    @Override
    public String toString() {
        return income +
                "," + outlay +
                "," + categoryStringId+
                "," + transactionCount;
    }
}
