package com.piedpiper_elte.piedwallet;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

@Dao
public interface TransactionDao {

    @Query("SELECT * FROM transactionentity")
    List<TransactionEntity> getAll();

    @Query("SELECT * FROM transactionentity WHERE id IN (:ids)")
    List<TransactionEntity> loadAllByIds(int[] ids);

    @Query("SELECT * FROM transactionentity WHERE title LIKE :title LIMIT 1")
    TransactionEntity findByTitle(String title);

    @Insert
    void insertAll(TransactionEntity... transactionEntities);

    @Delete
    void delete(TransactionEntity transactionEntity);
}
