package com.piedpiper_elte.piedwallet;


import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import static com.piedpiper_elte.piedwallet.Utils.generateHtmlTemplate;

public class TransactionCategorySumAdapter extends ArrayAdapter<TransactionCategorySum> {
    private Context context;
    private int layoutResourceId;
    private List<TransactionCategorySum> objects;

    public TransactionCategorySumAdapter(@NonNull Context context, int resource, @NonNull List<TransactionCategorySum> objects) {
        super(context, resource, objects);
        this.layoutResourceId = resource;
        this.context = context;
        this.objects = objects;
    }

    /**
     * Android által meghívott metódus, egy-egy ListView sort tölt fel adatokkal
     * @param position Android által kezelt paraméter
     * @param convertView Android által kezelt paraméter
     * @param parent Android által kezelt paraméter
     * @return létrehozott sor View
     */
    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View row = convertView;
        TransactionCategorySumAdapter.TransactionHolder holder;

        if(row == null)
        {
            LayoutInflater inflater = ((Activity)context).getLayoutInflater();
            row = inflater.inflate(layoutResourceId, parent, false);

            holder = new TransactionCategorySumAdapter.TransactionHolder();
            holder.icon = row.findViewById(R.id.icon);
            holder.category = row.findViewById(R.id.category);
            holder.income = row.findViewById(R.id.income);
            holder.outlay = row.findViewById(R.id.outlay);
            holder.sum = row.findViewById(R.id.sum);
            holder.constraintlayout = row.findViewById(R.id.constraintlayout);
            row.setTag(holder);
        }
        else
        {
            holder = (TransactionCategorySumAdapter.TransactionHolder)row.getTag();
        }

        TransactionCategorySum transaction = objects.get(position);
        holder.category.setText(context.getResources().getString(transaction.getCategoryStringId()));

        holder.sum.setText(Html.fromHtml(generateHtmlTemplate(transaction.getSum(), true)));
        holder.income.setText(Html.fromHtml(generateHtmlTemplate(transaction.getIncome(), true)));
        holder.outlay.setText(Html.fromHtml(generateHtmlTemplate(transaction.getOutlay(), true)));

        holder.icon.setImageResource(transaction.getCategory().getDrawableId());
        if(transaction.isOutlayable())
        {holder.constraintlayout.setBackgroundColor(Color.parseColor("#ffcccc"));}
        else
        {holder.constraintlayout.setBackgroundColor(Color.parseColor("#ccffcc"));}

        /*if(transaction.isSelected()) {
            row.findViewById(R.id.constraintlayout).setBackgroundColor(context.getResources().getColor(R.color.selectedRow));
        } else {
            row.findViewById(R.id.constraintlayout).setBackgroundColor(0); // Remove selected bg color
        }*/

        return row;
    }

    static class TransactionHolder
    {
        ImageView icon;
        TextView category;
        TextView income;
        TextView outlay;
        TextView sum;
        ConstraintLayout constraintlayout;
    }
}
