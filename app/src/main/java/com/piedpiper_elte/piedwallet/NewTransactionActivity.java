package com.piedpiper_elte.piedwallet;

import android.arch.persistence.room.Room;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;

public class NewTransactionActivity extends AppCompatActivity {
    Toast amountErrorToast;

    /**
     * Tömböt gyárt a kategória nevekből
     * @return kategória nevek
     */
    private Object[] getCategoryNames() {
        Category[] categories = Category.class.getEnumConstants();

        ArrayList<String> categoryNames = new ArrayList<>();

        for (Category category: categories) {
            categoryNames.add(getResources().getString(category.getStringId()));
        }

        return categoryNames.toArray();
    }

    /**
     * Spinner feltöltése adatokkal
     * @param spinner spinner
     * @param objects adatok
     */
    private void setSpinnerAdapter(Spinner spinner, Object[] objects) {
        ArrayAdapter<Object> adapter = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_item, objects);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
    }

    /**
     * Létrehozáskor hívódik meg, inicializálja az UI-t
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_transaction);

        Spinner categorySpinner = findViewById(R.id.categorySpinner);
        Object[] categoryNames = getCategoryNames();

        setSpinnerAdapter(categorySpinner, categoryNames);

        amountErrorToast = Toast.makeText(this, R.string.amount_error, Toast.LENGTH_SHORT);

        EditText noteEditText = findViewById(R.id.noteEditText);
        noteEditText.requestFocus();
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(noteEditText, InputMethodManager.SHOW_IMPLICIT);
    }

    /**
     * Megadja a kategórianévhez tartozó név String ID-t
     * @param selectedItemString kategórianév
     * @return String ID
     */
    private int getCategoryStringId(String selectedItemString) {
        for (Category category : Category.values()) {
            if(getResources().getString(category.getStringId()).equals(selectedItemString)) {
                return category.getStringId();
            }
        }

        return Category.OTHERS.getStringId();
    }

    /**
     * Új tranzakció hozzáadása, hozzáadás gombra kattintásra hívódik meg
     * @param view
     */
    public void addNewTransaction(View view) {
        EditText noteEditText = findViewById(R.id.noteEditText);
        Spinner categorySpinner = findViewById(R.id.categorySpinner);
        EditText amountEditText = findViewById(R.id.amountEditText);

        AppDatabase db = Room.databaseBuilder(getApplicationContext(), AppDatabase.class, "database").allowMainThreadQueries().build();

        try {
            TransactionEntity entity = new TransactionEntity();
            int amount = Integer.parseInt(amountEditText.getText().toString());
            entity.setBalanceChange(amount);
            entity.setTitle(noteEditText.getText().toString());
            entity.setCategoryStringId(getCategoryStringId(categorySpinner.getSelectedItem().toString()));

            if(amount == 0) {
                throw new NumberFormatException("");
            }

            db.transactionDao().insertAll(entity);

            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
        }

        catch (NumberFormatException e) {
            amountErrorToast.show();
        }
    }
}
