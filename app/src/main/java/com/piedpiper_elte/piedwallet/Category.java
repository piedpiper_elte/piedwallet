package com.piedpiper_elte.piedwallet;

public enum Category {
    FOOD_DRINKS(R.drawable.ic_restaurant_black_24dp, R.string.food_drinks),
    SHOPPING(R.drawable.ic_shopping_basket_black_24dp, R.string.shopping),
    HOUSING(R.drawable.ic_home_black_24dp, R.string.housing),
    TRANSPORTATION(R.drawable.ic_directions_bus_black_24dp, R.string.transportation),
    VEHICLE(R.drawable.ic_directions_car_black_24dp, R.string.vehicle),
    LIFE_ENTERTAINMENT(R.drawable.ic_person_black_24dp, R.string.life_entertainment),
    COMMUNICATION(R.drawable.ic_desktop_windows_black_24dp, R.string.communication),
    FINANCIAL_EXPENSES(R.drawable.ic_account_balance_black_24dp, R.string.financial_expenses),
    INVESTMENTS(R.drawable.ic_trending_up_black_24dp, R.string.investments),
    INCOME(R.drawable.ic_attach_money_black_24dp, R.string.income),
    OTHERS(R.drawable.ic_menu_black_24dp, R.string.others);

    private final int drawableId;
    private final int stringId;

    /**
     * Kategória
     * @param drawableId a kategória ikonjának az ID-je
     * @param name a kategória név Stringjének az ID-je
     */
    Category(int drawableId, int name) {
        this.drawableId = drawableId;
        this.stringId = name;
    }

    public int getDrawableId() {
        return drawableId;
    }

    public int getStringId() {
        return stringId;
    }

    public static Category getByStringId(int stringId) {
        for(Category category : values()) {
            if(category.getStringId() == stringId) {
                return category;
            }
        }

        return null;
    }
}
