package com.piedpiper_elte.piedwallet;

import android.arch.persistence.room.Room;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import com.piedpiper_elte.piedwallet.fragments.CategoryFragment;
import com.piedpiper_elte.piedwallet.fragments.OverviewFragment;
import com.piedpiper_elte.piedwallet.fragments.TransactionsFragment;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private AppDatabase db;
    private ArrayList<TransactionEntity> transactionData = new ArrayList<>();
    private Menu actionMenu;
    private boolean hideDeleteButton = false;

    /**
     * Inicializál mindent, ami az alkalmazás alapjait alkotja: UI elemek, adatbázis
     * @param savedInstanceState Android által kezelt paraméter
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        Utils utils = new Utils();

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, NewTransactionActivity.class);
                startActivity(intent);
            }
        });

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.setCheckedItem(R.id.nav_transactions); // Transactions view is the default content

        db = Room.databaseBuilder(getApplicationContext(), AppDatabase.class, "database").allowMainThreadQueries().build();
        transactionData = (ArrayList<TransactionEntity>) db.transactionDao().getAll();
        showDefaultFragment();
    }

    /**
     * A jelenlegi fragmentet lecseréli az alapértelmezett fragmentre.
     */
    private void showDefaultFragment() {
        Bundle bundle = new Bundle();
        bundle.putSerializable("transactionData", transactionData);
        Fragment fragment = new TransactionsFragment();
        fragment.setArguments(bundle);
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.fragmentContainer, fragment)
                .commit();
    }

    /**
     * Vissza gomb kezelése, bezérja a nyitott Navigation Drawert / visszalép
     */
    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            this.moveTaskToBack(true);
        }
    }

    /**
     * Options Menu létrehozása
     * @param menu a létrehozandó menü
     * @return true
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        actionMenu = menu;

        if (hideDeleteButton) menu.findItem(R.id.action_delete).setVisible(false);
        return true;
    }

    /**
     * Létrehoz egy String List-et a tranzakciók összegezett alakjából
     * @param categories a tranzakciók kategóriánkénti összegzése egy List-ben
     * @return létrehozott String List
     */
    private ArrayList<String> createTransactionCategorySumStringList(ArrayList<TransactionCategorySum> categories) {
        ArrayList<String> ret = new ArrayList<String>();
        for (TransactionCategorySum act : categories) {
            ret.add(act.toString());
        }
        return ret;
    }

    /**
     * Tranzakciók listájából létrehoz egy listát, melyben a tranzakciók kategóriánként vannak összegezve
     * @param transactions tranzakciók listája
     * @return kategóriánként összegzett lista
     */
    private ArrayList<TransactionCategorySum> createTransactionCategorySumList(List<TransactionEntity> transactions) {
        Set<Integer> detectedCategories = new TreeSet<Integer>();
        ArrayList<TransactionCategorySum> transactionCategorySums = new ArrayList<TransactionCategorySum>();
        for (TransactionEntity act : transactions) {
            detectedCategories.add(act.getCategoryStringId());
        }
        for (Integer actCat : detectedCategories) {

            ArrayList<TransactionEntity> transactionCategory = new ArrayList<TransactionEntity>();

            for (TransactionEntity actTran : transactions) {
                if (actTran.getCategoryStringId() == actCat) {
                    transactionCategory.add(actTran);
                }
            }
            transactionCategorySums.add(new TransactionCategorySum(transactionCategory));
        }
        return transactionCategorySums;
    }

    /**
     * Navigációs gombok kezelő metódusa
     * @param item megnyomott gomb
     * @return true
     */
    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        // Handle navigation view item clicks here.
        Fragment fragment = null;
        FragmentManager fragmentManager = getSupportFragmentManager();
        hideDeleteButton = true; // By default, we don't want to show the delete action button

        int id = item.getItemId();
        if (id == R.id.nav_transactions) {
            hideDeleteButton = false;
            showDefaultFragment();
        } else if (id == R.id.nav_overview) {
            fragment = new OverviewFragment();
            Bundle bundle = new Bundle();
            bundle.putStringArrayList("transactionCategorySums", createTransactionCategorySumStringList(createTransactionCategorySumList(transactionData)));
            fragment.setArguments(bundle);
        } else if (id == R.id.nav_categories) {
            fragment = new CategoryFragment();
            Bundle bundle = new Bundle();
            bundle.putStringArrayList("transactionCategorySums", createTransactionCategorySumStringList(createTransactionCategorySumList(transactionData)));
            fragment.setArguments(bundle);
        } else if (id == R.id.nav_about) { // Névjegy dialog

            AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);

            // A megfelelő layout alkalmazása a dialoghoz
            LayoutInflater factory = LayoutInflater.from(MainActivity.this);
            final View view = factory.inflate(R.layout.about_dialog, null);
            builder.setView(view);

            // Dialog elkészítése, majd megjelenítése
            final AlertDialog alertDialog = builder.create();
            alertDialog.show();

            // Bezáró gomb beállítása
            Button closeButton = (Button) alertDialog.findViewById(R.id.aboutButton);
            closeButton.setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    alertDialog.dismiss();
                }
            });

        }

        if (fragment != null) {
            fragmentManager.beginTransaction()
                    .replace(R.id.fragmentContainer, fragment)
                    .addToBackStack(null)
                    .commit();
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
