package com.piedpiper_elte.piedwallet;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import static com.piedpiper_elte.piedwallet.Utils.generateHtmlTemplate;


public class TransactionAdapter extends ArrayAdapter<TransactionEntity> {

    private Context context;
    private int layoutResourceId;
    private List<TransactionEntity> objects;

    public TransactionAdapter(@NonNull Context context, int resource, @NonNull List<TransactionEntity> objects) {
        super(context, resource, objects);
        this.layoutResourceId = resource;
        this.context = context;
        this.objects = objects;
    }

    /**
     * Android által meghívott metódus, egy-egy ListView sort tölt fel adatokkal
     * @param position Android által kezelt paraméter
     * @param convertView Android által kezelt paraméter
     * @param parent Android által kezelt paraméter
     * @return létrehozott sor View
     */
    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View row = convertView;
        TransactionHolder holder;

        if(row == null)
        {
            LayoutInflater inflater = ((Activity)context).getLayoutInflater();
            row = inflater.inflate(layoutResourceId, parent, false);

            holder = new TransactionHolder();
            holder.icon = row.findViewById(R.id.icon);
            holder.title = row.findViewById(R.id.noteEditText);
            holder.category = row.findViewById(R.id.category);
            holder.balanceChange = row.findViewById(R.id.balance);

            row.setTag(holder);
        }
        else
        {
            holder = (TransactionHolder)row.getTag();
        }
        TransactionEntity transaction = objects.get(position);
        holder.title.setText(transaction.getTitle());
        holder.category.setText(context.getResources().getString(transaction.getCategoryStringId()));
        holder.balanceChange.setText(Html.fromHtml(generateHtmlTemplate(transaction.getBalanceChange(), true)));

        Category category = transaction.getCategory();
        System.out.println("cat " + transaction.getCategoryStringId() + " id " + category.getDrawableId());
        int drawableId = category.getDrawableId();

        holder.icon.setImageResource(drawableId);

        if(transaction.isSelected()) {
            row.findViewById(R.id.constraintlayout).setBackgroundColor(context.getResources().getColor(R.color.selectedRow));
        } else {
            row.findViewById(R.id.constraintlayout).setBackgroundColor(0); // Remove selected bg color
        }

        /*String text = holder.balanceChange.getText().toString();
        if(transaction.getBalanceChange() >= 0) {
            holder.balanceChange.setTextColor(Color.parseColor("#00cc00"));
            holder.balanceChange.setText("+"+text);
        } else {
            holder.balanceChange.setTextColor(Color.parseColor("#ff0000"));
            holder.balanceChange.setText("-"+text);
        }*/

        return row;
    }




    static class TransactionHolder
    {
        ImageView icon;
        TextView title;
        TextView category;
        TextView balanceChange;
    }
}